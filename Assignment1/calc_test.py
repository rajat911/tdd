import unittest

import calc
#Use mocks and other unit test constructs.
class TestCalc(unittest.TestCase):
    def test_sub(self):
        self.assertEqual(calc.sub(10, 8), 2)

    def test_add(self):
        self.assertEqual(calc.add(10,5), 15)
        self.assertEqual(calc.add(-10,5), -5)
        self.assertEqual(calc.add(-10,-5), -15)

    def test_mul(self):
        self.assertEqual(calc.mul(10, 5), 50)

    def test_divide(self):
        self.assertEqual(calc.divide(10,2), 5)

        self.assertRaises(ValueError, calc.divide, 10, 0)

        with self.assertRaises(ValueError): #Same as above one
            calc.divide(10,0)

    def test_fact(self):
        self.assertEqual(calc.fact(5), 120)
        self.assertEqual(calc.fact(6), 720)


if __name__ == '__main__':
    unittest.main()