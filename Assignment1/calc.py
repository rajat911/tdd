#Create a class and put these functions under the class.
def add(a, b):
    return a+b

def mul(a, b):
    return a*b

def divide(a, b):
    if b == 0:
        raise ValueError('Cannot divide by Zero, Sorry!')
    else:
        return a/b

def sub(a, b):
    return a-b

def fact(n):
    ans = 1
    for i in range(1, n+1):
        ans = ans * i
    return ans